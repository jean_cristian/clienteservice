package br.com.tokio.cliente.controller;

import br.com.tokio.cliente.dto.request.ClienteRequest;
import br.com.tokio.cliente.dto.response.ClienteResponse;
import br.com.tokio.cliente.factory.FactoryConverter;
import br.com.tokio.cliente.helper.ClienteHelper;
import br.com.tokio.cliente.service.ClienteService;
import io.swagger.annotations.ApiOperation;
import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/cliente")
public class ClienteController {

    private final static Logger LOGGER = LoggerFactory.getLogger(ClienteController.class);

    @Autowired
    ClienteService clienteService;

    @Autowired
    ClienteHelper clienteHelper;

    @Autowired
    private FactoryConverter factoryConverter;


    @ApiOperation(value = "create", notes = "This operation gets an entity", response = String.class)
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<ClienteRequest> create(@RequestBody final ClienteRequest request) throws Exception {
        try {
            clienteService.save(clienteHelper.factoryCliente(request));
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }

    }


    @ApiOperation(value = "list", notes = "This operation list all entities", response = String.class)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<Page<ClienteResponse>> list(@RequestParam(required = false) final Long startAge, final Long endAge,
                                               @SortDefault(value = "id", direction = Sort.Direction.ASC) final Pageable pageable) throws Exception {
        try {
            return new ResponseEntity<>(
                    factoryConverter.convertPageToResponse(clienteService.findByFilter(endAge, startAge, pageable), ClienteResponse.class),
                    HttpStatus.OK);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
            throw new Exception(e.getMessage(), e);
        }
    }
}
