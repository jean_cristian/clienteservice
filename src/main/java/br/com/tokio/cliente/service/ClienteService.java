package br.com.tokio.cliente.service;

import br.com.tokio.cliente.dto.response.ClienteResponse;
import br.com.tokio.cliente.entity.Cliente;
import br.com.tokio.cliente.repository.ClienteRepository;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("clienteServiceSD")
public class ClienteService implements CRUDService<Cliente, Long> {

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    public Cliente save(Cliente entity) throws ServiceException {
        return clienteRepository.save(entity);
    }

    @Override
    public void delete(Cliente entity) throws ServiceException {

    }

    @Override
    public void delete(Long id) throws ServiceException {

    }

    @Override
    public List<Cliente> saveAll(Iterable<Cliente> entities) throws ServiceException {
        return null;
    }

    @Override
    public Cliente findById(Long id) throws ServiceException {
        return null;
    }

    @Override
    public Iterable<Cliente> findAll() throws ServiceException {
        return null;
    }

    @Override
    public Page<Cliente> findByFilter(String filter, String value, Pageable pageable) throws ServiceException {
        return null;
    }

    public Page<Cliente> findAllByAgeLessThanEqualAndAgeGreaterThanEqual(Long endAge, Long startAge, final Pageable pageable) {
        Page<ClienteResponse> cr = findByFilter(endAge, startAge, pageable);
        return clienteRepository.findAllByAgeLessThanEqualAndAgeGreaterThanEqual(endAge, startAge, pageable);
    }

    public Page<ClienteResponse> findByFilter(Long endAge, Long startAge, final Pageable pageable) {
        List<ClienteResponse> listClienteResponse = new ArrayList<ClienteResponse>();
        ClienteResponse clienteResponse = new ClienteResponse();

        final Page<Cliente> pc = clienteRepository.findAllByAgeLessThanEqualAndAgeGreaterThanEqual(endAge, startAge, pageable);

        List<Cliente> age18until25 = pc.stream().filter(idades -> {
            return idades.getAge() >= 18 && idades.getAge() <= 25;
        }).collect(Collectors.toList());

        List<Cliente> age26until40 = pc.stream().filter(idades -> {
            return idades.getAge() >= 26 && idades.getAge() <= 40;
        }).collect(Collectors.toList());

        List<Cliente> ageAbove40 = pc.stream().filter(idades -> {
            return idades.getAge() > 40;
        }).collect(Collectors.toList());

        clienteResponse.setAge18until25(age18until25);
        clienteResponse.setAge26until40(age26until40);
        clienteResponse.setAgeAbove40(ageAbove40);

        listClienteResponse.add(clienteResponse);
        return new PageImpl<>(listClienteResponse, pageable, pc.getTotalElements());

    }
}
