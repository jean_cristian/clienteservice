package br.com.tokio.cliente.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class ClienteRequest {
    private Date birthDate;
    private String name;
}
