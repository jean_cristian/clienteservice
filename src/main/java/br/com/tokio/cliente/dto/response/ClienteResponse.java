package br.com.tokio.cliente.dto.response;

import br.com.tokio.cliente.entity.Cliente;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ClienteResponse {
    private List<Cliente> age18until25;
    private List<Cliente> age26until40;
    private List<Cliente> ageAbove40;
}
