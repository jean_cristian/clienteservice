# Tecnologias

**gradle** - Para gerenciar dependência.

**Lombok** - para produtividade, ele fica responsável por gerar os Getter e Setter; Builder; Construtor, entre outros.

**Springboot** - Com configurações rápidas, você consegue, disponibilizar uma aplicação baseada no Spring. No caso disponibilizamos uma API.

**swagger** - é usado , construir, documentar e usar serviços da Web RESTful

**H2** - H2 é um banco de dados relacional gerado em memoria durante a execução da aplicação


## Collection Postman

Para filtrar os clientes a partir de um range de data, utilizar a URL abaixo, lembrando que a API exibe apenas clientes com idades a partir de 18 anos.

http method: GET

http://localhost:8080/cliente?endAge=120&startAge=0&page=0&size=99999999

## Como Utilizar

Assim que salvar o projeto para sua máquina é preciso rodar o **gradle build** para baixar as dependências do projeto.

Com o projeto rodando, basta acessar o swagger da API para cadastrar novos clientes na URL abaixo:
http://localhost:8080/swagger-ui.html

## Arquitetura - Microserviçsos
